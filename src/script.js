/*!
 * jQuery.fitSlider - jQuery Plugin
 *
 * Author : Sakuya.S (@Sakunyo)
 * Date   : 2013-02-27T06:33:55
 * Version: 1.0
 * License: BSD
 */


(function($){
	$.fitSlider = function(elem , configs){
		// initialize.
		this.el = $(elem);

		this.clip = configs.clip;
		this.item = configs.item;
		this.items = configs.items;

		this.duration = configs.duration;
		this.buttonNext = configs.buttonNext || ".next";
		this.buttonPrev = configs.buttonPrev || ".prev";

		this.itemWidth = configs.itemWidth;

		this.innerWidth = this.itemWidth * this.items.length;
		this.isMove = false;

		this.initialize();
	};

	$.extend($.fitSlider.prototype, {
		initialize: function () {
			var this_ = this;

			this.item.css({
				width: this.innerWidth
			});

			this.timerFunction();

			/**
			 * Events
			 */
			$(window).on("resize", function () {
				this_.onResize();
			});
			this.el.find(this.buttonNext).on("click", function () {
				this_.onNext();
			});
			this.el.find(this.buttonPrev).on("click", function () {
				this_.onPrev();
			});
		},
		onResize: function () {
			var this_ = this;

			clearTimeout(this.timer);

			this.timer = setTimeout(function(){
				this_.timerFunction();
			}, 100);
		},
		onEnd: function () {
			this.isMove = false;
		},
		onNext: function () {
			var this_ = this;

			if (Math.abs(parseInt(this.item.css("marginLeft"), 10)) + this.clip.outerWidth() >= this.innerWidth) {
				return false;
			}

			if (this.isMove) return false;
			this.isMove = true;

			this.item.animate({
				marginLeft : "-=" + this.itemWidth
			}, this.duration, function () {
				this_.onEnd();
			});

			return false;
		},
		onPrev: function () {
			var this_ = this;

			if (this.getMergin() >= 0) {
				return false;
			}

			if (this.isMove) return false;
			this.isMove = true;

			this.item.animate({
				marginLeft : "+=" + this.itemWidth
			}, this.duration, function () {
				this_.onEnd();
			});

			return false;
		},
		getMergin: function () {
			return parseInt(this.item.css("marginLeft"), 10);
		},
		getWidth: function () {
			return Math.floor(this.el.width() / this.itemWidth);
		},
		setMaxWidth: function () {
			if (this.getWidth() > this.items.length) {
				this.viewWidth = this.itemWidth * this.items.length;
			} else {
				this.viewWidth = this.itemWidth * this.getWidth();
			}
			return this.viewWidth;
		},
		timerFunction: function () {
			this.setMaxWidth();

			this.clip.css({
				width: this.viewWidth
			});

			if (this.viewWidth >= this.innerWidth + this.getMergin()) {
				this.item.css({
					marginLeft: (this.innerWidth - this.viewWidth) * -1
				});
			}
		}
	});

	$.fn.fitSlider = function(option) {
		return this.each(function() {
			new $.fitSlider(this , option);
		});
	};
}(jQuery));
