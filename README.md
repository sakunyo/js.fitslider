jQuery.fitSlider - jQuery Plugin

Author : Sakuya.S (@Sakunyo)

Date   : 2013-02-27T06:33:55

Version: 1.0

License: BSD



	<div id="slide" class="slideView">
		<div class="clip">
			<ul class="item">
				<li>ITEM</li> <li>ITEM</li> <li>ITEM</li> <li>ITEM</li>
				<li>ITEM</li> <li>ITEM</li> <li>ITEM</li> <li>ITEM</li>
			</ul>
		</div>
		<a href="#" class="next">Next</a>
		<a href="#" class="prev">Prev</a>
	</div>


	<script type="text/javascript">
	(function(){

		var view = $("#slide");
		view.fitSlider({
			clip: view.find(".clip"), // Clipping jQuery Object
			item: view.find(".item"), // Item Container jQuery Object
			items: view.find("li"), // Item jQuery Object

			itemWidth: 60, // Item Width (px)
			duration: 100, // Slide Animation Duration (ms)
			buttonNext: ".next",
			buttonPrev: ".prev"
		});

	}());
	</script>
